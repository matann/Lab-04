# Lab-04

# Microservices Workshop
Lab 04: Create a CD pipeline with Release Management

---

## Create an endpoint for docker hub:

 ![Image 1](images/lab04-1.png)


## Create a Release management pipeline to deploy the application:

- Add the following artifacts:

```
Type: Azure Repositories
Repo: microservices-calculator-app
Alias: _microservices-calculator-app (default)
```
```
Type: Docker Hub
Repo: sum-service
```
```
Type: Docker Hub
Repo: subtraction-service
```
```
Type: Docker Hub
Repo: division-service
```
```
Type: Docker Hub
Repo: multiplication-service
```
```
Type: Docker Hub
Repo: ui-service
```

 ![Image 2](images/lab04-2.png)



## Configure each environment tasks:

- Use a deployment group phase:

![Image 3](images/lab04-3.png)

- Configure the following tasks:

```
Name: clean
Script: docker rm -f $(docker ps -a -q)
		docker rmi -f $(docker images -a -q)
```

```
Name: docker-compose
Script: docker-compose up -d --force-recreate
Advanced/WorkingDirectory: $(Agent.ReleaseDirectory)/_microservices-calculator-app
```